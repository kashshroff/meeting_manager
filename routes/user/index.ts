import express from 'express'
import { createUser, login, verifyUser } from '../../components/user'
import { auth } from '../../middleware/auth'

const router = express.Router()

router.post('/login', login)

router.post('/', createUser)
router.get('/', auth, verifyUser)

export default router