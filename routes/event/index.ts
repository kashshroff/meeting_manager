import { add, allEvents } from './../../components/event/index';
import express from 'express'
import { auth } from '../../middleware/auth'

const router = express.Router()

router.post('/add', auth, add)
router.get('/', auth, allEvents)


export default router