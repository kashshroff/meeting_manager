import { getInstallUrl, verify } from './../../components/zoom/index';
import express from 'express'
import { auth } from '../../middleware/auth'

const router = express.Router()

router.get('/install', getInstallUrl)
router.get('/oauth/callback', verify)


export default router