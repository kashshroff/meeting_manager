import React from 'react'
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/react'
import Settings from '../components/Settings/Settings'

const SettingsPage: React.FC = (props: any) => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>My Events</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <Settings history={props.history} auth={props.auth} />
            </IonContent>
        </IonPage>
    )
}

export default SettingsPage