import React from 'react';
import { IonContent, IonText, IonHeader, IonToolbar, IonTitle, IonButton, IonPage } from '@ionic/react';
import { IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel, IonBadge } from '@ionic/react';
import { calendar, personCircle, map, informationCircle } from 'ionicons/icons';
import { logoutUser } from '../actions/authActions';
import { connect, ConnectedProps } from 'react-redux'
import { LOGIN } from '../routes';

const mapStateToProps = (state: any) => ({
})

const mapDispatchToProps = (dispatch: Function) => ({
    logoutSuccess: () => dispatch(logoutUser())
})

type PropsFromRedux = ConnectedProps<typeof connector>;


const connector = connect(mapStateToProps, mapDispatchToProps);

class Home extends React.Component<any, any> {

    logout = () => {
        this.props.logoutSuccess()
        this.props.history.push(LOGIN)
    }

    render() {
        return (
            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonTitle>Home</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    This is home page
                </IonContent>
            </IonPage>
        )
    }
}


export default connector(Home)