import React from 'react'
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/react'
import AddEvents from '../components/Events/AddEvents'

const AddEventsPage: React.FC = (props: any) => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Add Events</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <AddEvents history={props.history} auth={props.auth} />
            </IonContent>
        </IonPage>
    )
}

export default AddEventsPage