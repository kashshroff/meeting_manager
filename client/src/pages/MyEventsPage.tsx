import React from 'react'
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/react'
import MyEvents from '../components/Events/MyEvents'

const MyEventsPage: React.FC = (props: any) => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>My Events</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <MyEvents history={props.history} auth={props.auth} />
            </IonContent>
        </IonPage>
    )
}

export default MyEventsPage