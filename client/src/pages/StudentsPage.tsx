import React from 'react'
import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/react'
import Students from '../components/Students/Students'

const StudentsPage: React.FC = (props: any) => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>My Events</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <Students history={props.history} />
            </IonContent>
        </IonPage>
    )
}

export default StudentsPage