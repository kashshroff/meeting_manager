import Axios from 'axios'
import { register } from '../../routes/api'

export const RegisterApi = async(email: string, password: string, name: string, phone: any) => {
    if(email == "" || password == "" || name == "" || phone == ""){
        return {
            "error": 1,
            "message": "Please fill all fields"
        }
    }else {
        try{
            let res = await Axios.post(register, {
                email,
                password,
                name,
                phone
            })
            if(res.data && !res.data.error){
                return res.data
            }else {
                return res
            }
        } catch (err) {
            return {
                error: true,
                message: "Email already exists"
            }
        }
        
        
    }
}