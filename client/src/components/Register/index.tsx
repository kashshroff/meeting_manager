import React from 'react';
import { IonGrid, IonInput, IonIcon, IonButton, IonText } from '@ionic/react';
import { mailOutline, keyOutline, personAdd, phonePortrait } from 'ionicons/icons';
import { RegisterApi } from './register'
import './register.css'
import { HOME, LOGIN } from '../../routes';
import { connect, ConnectedProps } from 'react-redux'
import { loadUser, loginUser, registerUser } from '../../actions/authActions';

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})

const mapDispatchToProps = (dispatch: Function) => ({
    user: () => dispatch(loadUser()),
    registerSuccess: (data: Object) => dispatch(registerUser(data))
})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class Register extends React.Component<any, any> {

    state = {
        email: "",
        name: "",
        phone: "",
        password: "",
        error: "",
        errorMsg: ""
    }

    async componentDidMount() {
        await this.props.user()
        let {isAuthenticated} = this.props.auth
        if(isAuthenticated){
            console.log("Authenticated - Register")
            this.props.history.push(HOME)
        }
    }

    componentDidUpdate(){
        let {isAuthenticated} = this.props.auth
        if(isAuthenticated){
            console.log("Authenticated - Login")
            this.props.history.push(HOME)
        }
    }

    register = async() => {
        this.setState({
            error: false,
            errorMsg: ""
        })
        
        let {email, password, name, phone} = this.state
        let res: any = await RegisterApi(email, password, name, phone)
        console.log(res)
        if(!res.error){
            // Redux action dispatch
            this.props.history.push(LOGIN)
        }else {
            this.setState({
                error: true,
                errorMsg: res.message
            })
        }
    }


    render() {

        let { email, password, error, errorMsg, name, phone } = this.state

        return (
            <IonGrid className="login login-container">
                <div className="align-center">
                    <IonInput
                        autofocus={true}
                        inputmode="text"
                        value={name}
                        onIonChange={(e) => this.setState({ name: e.detail.value })}
                        placeholder="Enter Full Name"
                    ><IonIcon icon={personAdd}></IonIcon></IonInput>
                    <IonInput
                        autofocus={true}
                        inputmode="tel"
                        value={phone}
                        onIonChange={(e) => this.setState({ phone: e.detail.value })}
                        placeholder="Enter Phone number"
                    ><IonIcon icon={phonePortrait}></IonIcon></IonInput>
                    <IonInput
                        autofocus={true}
                        inputmode="email"
                        value={email}
                        onIonChange={(e) => this.setState({ email: e.detail.value })}
                        placeholder="Enter email address"
                    ><IonIcon icon={mailOutline}></IonIcon></IonInput>
                    <IonInput
                        type="password"
                        value={password}
                        onIonChange={(e) => this.setState({ password: e.detail.value })}
                        placeholder="Enter password"
                    ><IonIcon icon={keyOutline}></IonIcon></IonInput>
                    <IonButton
                        color="primary"
                        expand="block"
                        onClick={() => this.register()}
                    >Register</IonButton>
                    {error && <IonText className="text_danger">{errorMsg}</IonText>}
                </div>
            </IonGrid>
        )
    }
}


export default connector(Register)