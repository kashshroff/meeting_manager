import React from 'react';
import { IonGrid, IonInput, IonIcon, IonButton, IonText, IonList, IonItem } from '@ionic/react';
import { connect, ConnectedProps } from 'react-redux'
import Axios from 'axios';
import { installZoom } from './zoom';

const mapStateToProps = (state: any) => ({
    
})

const mapDispatchToProps = (dispatch: Function) => ({
    
})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class Settings extends React.Component<any, any> {

    state = {
        
    }

    verifyWithZoom = async() => {
        let res = await installZoom()
        if(!res.error){
            let url = res.url + "&state="+this.props.auth.user.id
            console.log(url)
            window.location.href = url
        }
    } 

    render(){

        let {user} = this.props.auth
        
        return(
            <IonGrid>
                <IonList>
                    <IonItem>
                        {!user.zoom_access_token && <IonButton onClick={() => this.verifyWithZoom()}>Connect Zoom</IonButton>}
                        {user.zoom_access_token && <IonButton onClick={() => {}} color="danger">Disconnect Zoom</IonButton>}
                        
                    </IonItem>
                    <IonItem>
                        <IonButton>Log out</IonButton>
                    </IonItem>
                </IonList>
            </IonGrid>
        )
    }
}


export default connector(Settings)