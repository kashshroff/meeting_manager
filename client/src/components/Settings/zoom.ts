import Axios from "axios"

export const installZoom = async() => {
    try {
        let res = await Axios.get("/api/zoom/install")
        return res.data
        
    } catch(err) {
        return {
            error: true,
            message: "Zoom not configured."
        }
    }
}