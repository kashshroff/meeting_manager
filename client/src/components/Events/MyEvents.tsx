import React from 'react';
import { IonGrid, IonInput, IonIcon, IonButton, IonText, IonRow, IonItem, IonLabel } from '@ionic/react';
import { connect, ConnectedProps } from 'react-redux'
import { getAllEvents } from './operations';

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = (dispatch: Function) => ({

})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class MyEvents extends React.Component<any, any> {

    state = {
        loading: true,
        events: []
    }

    async componentDidMount() {
        let { user, token } = this.props.auth
        let events: any = await getAllEvents(user.id, token)
        if (!events.error) {
            this.setState({
                events: events.data,
                loading: false
            })
        }
    }

    render() {

        let { loading, events } = this.state
        console.log(events)

        if (loading) {
            return ("Loading")
        }

        return (
            <IonGrid className="">
                {events && events.length && events.map((event: any) => {
                    return (
                        <IonRow>
                            <IonText>{event.title}</IonText>
                                <br />
                            <IonItem>
                                
                                <IonText>URL: {event.meeting_url}</IonText>
                                
                            </IonItem>
                        </IonRow>
                    )
                })

                }
                {(!events || !events.length) &&
                    "No Events Present"
                }

            </IonGrid>
        )
    }
}


export default connector(MyEvents)