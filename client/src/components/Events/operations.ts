import { allevent } from './../../routes/api';
import { commonHeaders } from './../../constants/headers';
import Axios from 'axios';
import {addevent} from '../../routes/api'


export const addEvent = async(title: any, description: any, date: any, amount: any, token: any) => {
    if(title == "" || description == "" || date == ""){
        return {
            error: true,
            message: "Please fill all fields"
        }
    }

    let headers: any = commonHeaders()
    headers['x-auth-token'] = token

    try{
        let res = await Axios.post(addevent, {
            title, description, date, amount
        }, {headers})

        if(!res.data.error){
            return {
                error: false
            }
        }

    } catch(err){
        return {
            error: true,
            message: "Internal server error. Please try again later"
        }
    }
    
}

export const getAllEvents = async(user_id: any, token: any) => {
    let headers: any = commonHeaders()
    headers['x-auth-token'] = token

    try{
        let res = await Axios.get(allevent, {headers})

        if(!res.data.error){
            return {
                error: false,
                data: res.data.data
            }
        }

    } catch(err){
        return {
            error: true,
            message: "Internal server error. Please try again later"
        }
    }
}