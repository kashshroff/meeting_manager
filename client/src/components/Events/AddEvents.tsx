import React from 'react';
import { IonGrid, IonInput, IonIcon, IonButton, IonText, IonTextarea, IonRow, IonCol, IonItem, IonLabel, IonCheckbox, IonDatetime } from '@ionic/react';
import { connect, ConnectedProps } from 'react-redux'
import moment from 'moment'
import { addEvent } from './operations';
import { EVENTSMY } from '../../routes';

const mapStateToProps = (state: any) => ({

})

const mapDispatchToProps = (dispatch: Function) => ({

})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class AddEvents extends React.Component<any, any> {

    state = {
        title: "",
        description: "",
        date: "",
        time: "",
        amount: 0,
        free: false,
        loading: false
    }

    saveEvent = async() => {
        let {title, description, date, time, free, amount} = this.state
        let {token} = this.props.auth
        let new_date = date.split("T")[0] + " " + moment(this.state.time).format("HH:mm") + ":00"
        let res: any = addEvent(title, description, new_date, amount, token)
        if(!res.error){
            this.props.history.push(EVENTSMY)
        }
    }

    render() {
        let {title, description, date, time, free, amount} = this.state

        return (
            <IonGrid className="">
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonInput
                                placeholder="Event Name"
                                required={true}
                                size={50}
                                type="text"
                                value={title}
                                onIonChange={(e) => this.setState({title: e.detail.value})}
                            >
                            </IonInput>
                        </IonItem>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonTextarea
                                placeholder="Enter description of event here..."
                                rows={5}
                                value={description}
                                onIonChange={(e) => this.setState({description: e.detail.value})}
                            ></IonTextarea>
                        </IonItem>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonLabel>DD MM YYYY</IonLabel>
                            <IonDatetime 
                                displayFormat="DD MM YYYY" 
                                placeholder="Select Date"
                                value={date}
                                onIonChange={e => this.setState({date: e.detail.value})}
                            ></IonDatetime>
                        </IonItem>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonLabel>HH MM</IonLabel>
                            <IonDatetime 
                                displayFormat="hh:mm A" 
                                placeholder="Select Date"
                                value={time}
                                onIonChange={e => this.setState({time: e.detail.value})}
                            ></IonDatetime>
                        </IonItem>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonCheckbox
                                slot="start"
                                color="dark"
                                checked={free}
                                onIonChange={() => this.setState({free: !free})}
                            />
                            <IonLabel>Is event free ?</IonLabel>
                        </IonItem>
                    </IonCol>
                </IonRow>
                {!free && 
                <IonRow>
                    <IonCol>
                        <IonItem>
                            <IonInput
                                placeholder="Price"
                                size={50}
                                type="number"
                                value={amount}
                                onIonChange={e => this.setState({amount: e.detail.value})}
                            >
                            </IonInput>
                        </IonItem>
                    </IonCol>
                </IonRow>
                }
                <IonRow>
                    <IonCol>
                        <IonButton onClick={() => this.saveEvent()}>Save</IonButton>
                    </IonCol>
                </IonRow>
            </IonGrid>
        )
    }
}


export default connector(AddEvents)