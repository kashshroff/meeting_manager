import Axios from 'axios'
import { login } from '../../routes/api'

export const LoginApi = async(email: string, password: string) => {
    if(email == "" || password == ""){
        return {
            "error": 1,
            "message": "Email/Password is incomplete"
        }
    }else {
        console.log("else")
        let res = await Axios.post(login, {
            email,
            password
        })
        if(res.data && !res.data.error){
            return res.data
        }else {
            return res
        }
    }
}