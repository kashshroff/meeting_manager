import React from 'react';
import { IonGrid, IonInput, IonIcon, IonButton, IonText } from '@ionic/react';
import { mailOutline, keyOutline } from 'ionicons/icons';
import { LoginApi } from './login'
import './login.css'
import { HOME, REGISTER } from '../../routes';
import { connect, ConnectedProps } from 'react-redux'
import { loadUser, loginUser } from '../../actions/authActions';

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})

const mapDispatchToProps = (dispatch: Function) => ({
    user: () => dispatch(loadUser()),
    loginSuccess: (data: Object) => dispatch(loginUser(data))
})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class Login extends React.Component<any, any> {

    state = {
        email: "",
        password: "",
        error: "",
        errorMsg: ""
    }

    async componentDidMount() {
        await this.props.user()
    }

    componentDidUpdate(){
        let {isAuthenticated} = this.props.auth
        if(isAuthenticated){
            console.log("Authenticated - Login")
            this.props.history.push(HOME)
        }
    }

    login = async () => {
        this.setState({
            error: false,
            errorMsg: ""
        })
        
        let {email, password} = this.state
        let res: any = await LoginApi(email, password)
        if(!res.error){
            // Redux action dispatch
            let {error, message, ...rest} = res
            this.props.loginSuccess({...rest})
            this.props.history.push(HOME)
        }else {
            this.setState({
                error: true,
                errorMsg: res.message
            })
        }
    }
    
    render(){

        let {email, password, error, errorMsg} = this.state

        return(
            <IonGrid className="login login-container">
                <div className="align-center">
                    <IonInput
                        autofocus={true}
                        inputmode="email"
                        value={email}
                        onIonChange={(e) => this.setState({email: e.detail.value})}
                        placeholder="Enter email address"
                    ><IonIcon icon={mailOutline}></IonIcon></IonInput>
                    <IonInput
                        type="password"
                        value={password}
                        onIonChange={(e) => this.setState({password: e.detail.value})}
                        placeholder="Enter password"
                    ><IonIcon icon={keyOutline}></IonIcon></IonInput>
                    <IonButton 
                        color="primary"
                        expand="block"
                        onClick={() => this.login()}
                    >Log in</IonButton>
                    {error && <IonText className="text_danger">{errorMsg}</IonText>}
                    <IonButton 
                        color="dark"
                        expand="block"
                        onClick={() => this.props.history.push(REGISTER)}
                    >Register</IonButton>
                </div>
            </IonGrid>
        )
    }
}


export default connector(Login)