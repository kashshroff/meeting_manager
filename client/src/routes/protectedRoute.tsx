import React from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { Route } from 'react-router'
import {loadUser} from '../actions/authActions'
import { Redirect } from 'react-router-dom'

const mapStateToProps = (state: any) => ({
    auth: state.auth,
})

const mapDispatchToProps = (dispatch: Function) => ({
    user: () => dispatch(loadUser())
})

type PropsFromRedux = ConnectedProps<typeof connector>;

const connector = connect(mapStateToProps, mapDispatchToProps);

class ProtectedRoute extends React.Component <any, any> {

    async componentDidMount(){
        let { user } = this.props
        await user()
    }

    render(){
        const Component = this.props.component
        let {component, ...restProps} = this.props
        let {isAuthenticated, isLoading, user, token} = this.props.auth
        return isAuthenticated ? (
            <Route {...restProps} render={
                (props) => {
                    return <Component auth={{user: user.data, token}} {...props} />
                }
            } />
            
        ) : <Redirect to={{ pathname: '/login' }} />
       
    }
}


export default connector(ProtectedRoute)