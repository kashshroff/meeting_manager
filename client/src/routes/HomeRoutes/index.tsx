import React from 'react'
import { IonTabs, IonRouterOutlet, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
import ProtectedRoute from '../protectedRoute';
import { HOME, EVENTSMY, EVENTSADD, STUDENTS, SETTINGS } from '..';
import Home from '../../pages/Home'
import RegisterPage from '../../pages/Register';
import { Route } from 'react-router';
import { calendarOutline, addCircleOutline, settingsOutline, bodyOutline, homeOutline } from 'ionicons/icons';
import MyEventsPage from '../../pages/MyEventsPage';
import AddEventsPage from '../../pages/AddEventsPage';
import SettingsPage from '../../pages/SettingsPage';
import StudentsPage from '../../pages/StudentsPage';

const HomeRoutes: React.FC = () => {
  return (
    <IonTabs>
      <IonRouterOutlet>
        <ProtectedRoute path={EVENTSMY} component={MyEventsPage} exact={true} />
        <ProtectedRoute path={EVENTSADD} component={AddEventsPage} exact={true} />
        <ProtectedRoute path={STUDENTS} component={StudentsPage} exact={true} />
        <ProtectedRoute path={SETTINGS} component={SettingsPage} exact={true} />
        <ProtectedRoute path={HOME} component={Home} exact={true} />
        <ProtectedRoute path="/" component={Home} exact={true} />
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="myEvents" href={EVENTSMY}>
          <IonIcon icon={calendarOutline} />
          <IonLabel>My Events</IonLabel>
        </IonTabButton>
        <IonTabButton tab="addEvents" href={EVENTSADD}>
          <IonIcon icon={addCircleOutline} />
          <IonLabel>Add Event</IonLabel>
        </IonTabButton>
        <IonTabButton tab="home" href={HOME}>
          <IonIcon icon={homeOutline} />
          <IonLabel>Home</IonLabel>
        </IonTabButton>
        <IonTabButton tab="students" href={STUDENTS}>
          <IonIcon icon={bodyOutline} />
          <IonLabel>Students</IonLabel>
        </IonTabButton>
        <IonTabButton tab="settings" href={SETTINGS}>
          <IonIcon icon={settingsOutline} />
          <IonLabel>Settings</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
};

export default HomeRoutes;