import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { IonApp } from '@ionic/react';
import './App.css'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import { Provider } from 'react-redux'
import store from './store'




import LoginPage from './pages/Login'
import { LOGIN, REGISTER } from './routes';
import RegisterPage from './pages/Register';


import { IonReactRouter } from '@ionic/react-router';
import HomeRoutes from './routes/HomeRoutes';
import { loadUser } from './actions/authActions';
import { connect } from 'react-redux';
import { getToken } from './utils';

class App extends React.Component<any, any> {

  componentDidMount() {
    
  }

  render() {
    let token = getToken()
    console.log(token)
    return (
      <Provider store={store}>
        <IonApp>
          <IonReactRouter>
            <Switch>
              <Route path={LOGIN} component={LoginPage} exact={true} />
              <Route path={REGISTER} component={RegisterPage} exact={true} />
              <Route path="/" component={token ? HomeRoutes : LoginPage} />
            </Switch>
          </IonReactRouter>
        </IonApp>
      </Provider>
    )
  }

}

export default App;