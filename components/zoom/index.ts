import { updateUserZoomCreds } from './../../Database/User/index';
import Axios from 'axios';
import Database from '../../Database/Database'

let db = new Database()

export const getInstallUrl = (req: any, res: any) => {

    if(!process.env.ZOOM_CLIENT_ID || !process.env.ZOOM_REDIRECT_URI){
        res.status(404).send({
            error: true,
            message: "Zoom not configured on server"
        })
    }

    let url = "https://zoom.us/oauth/authorize?response_type=code&client_id="+process.env.ZOOM_CLIENT_ID+"&redirect_uri="+process.env.ZOOM_REDIRECT_URI
    res.send({
        error: false,
        url
    })
}

export const verify = async(req: any, res: any) => {
    let { state, code } = req.query
    
    let headers = {
        "Authorization": "Basic "+Buffer.from(process.env.ZOOM_CLIENT_ID+":"+process.env.ZOOM_SECRET_KEY).toString('base64')
    }

    try{
        let zoom = await Axios.post("https://zoom.us/oauth/token?grant_type=authorization_code&code="+code+"&redirect_uri="+process.env.ZOOM_REDIRECT_URI, 
            {}, 
            {headers}
        )
        console.log(zoom.data)
        if(zoom.data.access_token){
            console.log("Inside true condition", state)
            let result: any = await db.execute(updateUserZoomCreds(state, zoom.data.access_token, zoom.data.refresh_token))
            console.log(result)
            if(result.rowCount){
                res.redirect('http://localhost:8100/home')
            }
        }

    } catch(err){
        console.log("ZOOM ERR", err.toJSON())
    }
    
}

export const createMeeting = async(data: any, token: any, refresh: any, id: any) => {
    console.log("Creating meeting")
    try{
        let zoomRes: any = await Axios.post("https://api.zoom.us/v2/users/me/meetings", data, {
            headers: {
                Authorization: "Bearer "+token
            }
        })
        console.log(zoomRes)
        return zoomRes.data
    }catch(err){
        console.log("error to find access token")
        console.log(err.response.status)
        if(err.response.status == 401){
            console.log("Code matched")
            let newToken = await refreshAccessToken(refresh, id)
            console.log("NewToken", newToken)
            createMeeting(data, newToken.access_token, newToken.refresh_token, id)
        }
    }
}

export const refreshAccessToken = async(refresh_token: any, id: any) => {
    console.log("Refreshing access token")
    let headers = {
        "Authorization": "Basic "+Buffer.from(process.env.ZOOM_CLIENT_ID+":"+process.env.ZOOM_SECRET_KEY).toString('base64')
    }
    try{
        let zoom: any = await Axios.post("https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token="+refresh_token, {}, {
            headers
        })
        console.log(zoom)
        if(zoom.data.access_token){
            let result: any = await db.execute(updateUserZoomCreds(id, zoom.data.access_token, zoom.data.refresh_token))
            return zoom.data
        }
    }catch(err){
        console.log("Refresh catch", err.response.data)
    }
}