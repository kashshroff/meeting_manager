import { createEvent, myEvents } from './../../Database/Event/index';
import { createMeeting } from './../zoom/index';
import Database from '../../Database/Database'
import Axios from 'axios';

let db = new Database()

export const add = async(req: any, res: any) => {
    console.log(req.body)

    let {title, description, date, amount} = req.body
    let {zoom_access_token, zoom_refresh_token, id} = req.user
    console.log(req.user)

    let data_for_meeting = {
        "topic": title,
        "password": "asdasd",
        "start_time": date.replace(" ", "T"),
        "type": 2
    }

    let zoom: any = await createMeeting(data_for_meeting, zoom_access_token, zoom_refresh_token, id)

    let result: any = await db.execute(createEvent(title, description, date, amount, zoom.join_url, JSON.stringify(zoom), id))

    res.send({
        error: false
    })
}

export const allEvents = async(req: any, res: any) => {
    let {id} = req.user
    try{
        let result: any = await db.execute(myEvents(id))
        if(result.rowCount){
            res.send({
                error: false,
                data: result.rows
            })
        }
    }catch(err) {
        res.status(400).send({
            error: true,
            data: "Error"
        })
    }
    
}