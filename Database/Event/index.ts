import { escape } from 'mysql';
import { EVENTS } from './../Tables';

export const createEvent = (title: string, description: string, date: string, amount: any, meeting_url: string, additional_details: any, user_id: any) => {
    return `
        INSERT INTO ${EVENTS} (title, description, date, amount, meeting_url, additional_details, user_id) VALUES 
        (${escape(title)}, ${escape(description)}, ${escape(date)}, ${escape(amount)}, ${escape(meeting_url)}, ${escape(additional_details)}, ${escape(user_id)})
    `
}

export const myEvents = (user_id: any) => {
    return `
        SELECT * FROM ${EVENTS} WHERE user_id = ${user_id}
    `
}