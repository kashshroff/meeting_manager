import sqlConnector from '../utils/sqlConnector'

export default class DataBase {
    manageConnections(query: string) {
        return new Promise((resolve, reject) => {
            let connection = sqlConnector()

            //create a query
            connection.query(query, function (error, results, fields) {
                if (error) {
                    console.log(error)
                    reject(error)
                    return
                }
                // connected!
                const rows = results
                const rowCount = results.length ? results.length : results.affectedRows

                resolve({ rows, rowCount })
            })
            connection.end()
        })
    }

    async execute(query: string) {
        let result = await this.manageConnections(query)
        return result
    }
}
