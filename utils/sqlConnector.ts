import mysql from 'mysql'

export default function mySqlDbConnection() {
   const connection = mysql.createConnection({
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
   })

   connection.connect()
   return connection
}