import express from 'express'
import * as dotenv from 'dotenv'
//For environment variables
dotenv.config()

import User from './routes/user'
import Zoom from './routes/zoom'
import Event from './routes/event'

const app = express()

// Body Parser middleware
app.use(express.json())

// User Routes
app.use('/api/user', User)
app.use('/api/zoom', Zoom)
app.use('/api/event', Event)


const port = process.env.PORT || 5000 

app.listen(port, () => console.log(`Server started on port : ${port}`))